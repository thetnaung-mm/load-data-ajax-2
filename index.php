<?php
	//Database config detail
	define('_HOST_NAME','localhost');
    define('_DATABASE_NAME','tbl_content');
    define('_DATABASE_USER_NAME','root');
    define('_DATABASE_PASSWORD','');
 
    $dbConnection = new mysqli(_HOST_NAME, _DATABASE_USER_NAME, _DATABASE_PASSWORD, _DATABASE_NAME);
    if ($dbConnection->connect_error) {
          trigger_error('Connection Failed: '  . $dbConnection->connect_error, E_USER_ERROR);
    }
	 
	//item per page
	$limit = 5; 
	$page =(int)(!isset($_GET['p']))?1: $_GET['p'];
 
	// sql query
	$sqlContent='SELECT * FROM tbl_content';
	
	//Query start point
	$start =($page * $limit)- $limit;
 
	$resContent=$dbConnection->query($sqlContent);
	$rows_returned = $resContent->num_rows;
	
	// query for page navigation
	if( $rows_returned > ($page * $limit)){
		$next =++$page;
	}
	
	$sqlContent = $sqlContent ." LIMIT $start, $limit";
	$finalContent = $dbConnection->query($sqlContent);
	if($finalContent === false) {
		trigger_error('Error: ' . $dbConnection->error, E_USER_ERROR);
	} else {
		$rows_returned = $finalContent->num_rows;
	}
?>
<!doctype html>
<html>
<head>
<meta http-equiv="Content-Type"content="text/html; charset=UTF-8">
<title>How To Load Data On Page Scroll Using JQuery</title>
</head>
<body>
<div class='tLink'><strong>Tutorial Link:</strong> <a href='http://www.stepblogging.com/how-to-load-data-on-page-scroll-using-jquery/'target='_blank'>Click Here</a></div><br/>
<div class='web'>
	<h1>How To Load Data On Page Scroll Using JQuery</h1> <br />
	<div class="wrap">
    <!-- loop row data -->
    <?php while($rowContent = $finalContent->fetch_assoc()) { ?>
        <div class="item" id="item-<?php echo $rowContent['id']?>">
            <h2><?php echo $rowContent['title'] ?></h2>
            <p><?php echo $rowContent['description']?></p>
        </div>
    <?php } ?>
 
    <!--page navigation-->
    <?php if(isset($next)):?>
        <div class="nav">
            <a href='index.php?p=<?php echo $next?>'>Next</a>
        </div>
    <?php endif ?>
    </div>
</div>
</body>
</html>

<script src="jquery-2.0.min.js"></script>
<script type="text/javascript"src="jquery-ias.js"></script>
<script type="text/javascript">
  jQuery(document).ready(function(){
    // Infinite Ajax Scroll configuration
    jQuery.ias({
      container :'.wrap', // main container where data goes to append
      item:'.item', // single items
      pagination:'.nav', // page navigation
      next:'.nav a' // next page selector
    })
    .extension(new IASSpinnerExtension())
    .extension(new IASTriggerExtension({offset: 5}));
});
</script>
<style>
.web{
	font-family:tahoma;
	size:12px;
	top:10%;
	border:1px solid #CDCDCD;
	border-radius:10px;
	padding:10px;
	width:45%;
	margin:auto;
	height:auto;
}
h1, h2{
	margin:3px 0;
	font-size:13px;
	text-decoration:underline;
}
.tLink{
	font-family:tahoma;
	size:12px;
	padding-left:10px;
	text-align:center;
}

.talign_right{
	text-align:right;
}
.username_availability_result{
	display:block;
	width:auto;
	float:left;
	padding-left:10px;
}
.input{
	float:left;
}
p{
	font-family:tahoma;
	font-size:13px;
}
a{
	font-size:13px;
	color:#FF0000;
}

</style>